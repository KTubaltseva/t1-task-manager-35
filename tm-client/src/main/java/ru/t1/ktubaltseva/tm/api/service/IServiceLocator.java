package ru.t1.ktubaltseva.tm.api.service;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.endpoint.*;

public interface IServiceLocator {

    @NotNull
    IAuthEndpoint getAuthEndpoint();

    @NotNull
    ICommandService getCommandService();

    @NotNull
    IDomainEndpoint getDomainEndpoint();

    @NotNull
    ILoggerService getLoggerService();

    @NotNull
    IProjectEndpoint getProjectEndpoint();

    @NotNull
    IPropertyService getPropertyService();

    @NotNull
    ISystemEndpoint getSystemEndpoint();

    @NotNull
    ITaskEndpoint getTaskEndpoint();

    @NotNull
    ITokenService getTokenService();

    @NotNull
    IUserEndpoint getUserEndpoint();


}
