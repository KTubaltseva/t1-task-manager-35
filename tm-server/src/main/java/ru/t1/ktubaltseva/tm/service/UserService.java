package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.EmailAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityAlreadyExists.LoginAlreadyExistsException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.List;

public final class UserService extends AbstractService<User, IUserRepository> implements IUserService {

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IPropertyService propertyService;

    public UserService(
            @NotNull final IUserRepository modelRepository,
            @NotNull final ITaskRepository taskRepository,
            @NotNull final IProjectRepository projectRepository,
            @NotNull final IPropertyService propertyService
    ) {
        super(modelRepository);
        this.taskRepository = taskRepository;
        this.projectRepository = projectRepository;
        this.propertyService = propertyService;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(Role.USUAL);
        return modelRepository.add(user);
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        if (isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        if (isEmailExists(login)) throw new EmailAlreadyExistsException(email);
        @NotNull final User user = create(login, password);
        user.setEmail(email);
        return user;
    }

    @NotNull
    @Override
    public User create(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final Role role
    ) throws AbstractException, NoSuchAlgorithmException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        if (role == null) throw new RoleEmptyException();
        if (modelRepository.isLoginExists(login)) throw new LoginAlreadyExistsException(login);
        @NotNull final User user = create(login, password);
        user.setRole(role);
        return user;
    }

    @NotNull
    @Override
    public User findByLogin(@Nullable final String login) throws LoginEmptyException, UserNotFoundException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        @Nullable final User user = modelRepository.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public User findByEmail(@Nullable final String email) throws EmailEmptyException, UserNotFoundException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        @Nullable final User user = modelRepository.findByEmail(email);
        if (user == null) throw new UserNotFoundException();
        return user;
    }

    @NotNull
    @Override
    public Boolean isLoginExists(@Nullable final String login) throws LoginEmptyException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        return modelRepository.isLoginExists(login);
    }

    @NotNull
    @Override
    public Boolean isEmailExists(@Nullable final String email) throws EmailEmptyException {
        if (email == null || email.isEmpty()) throw new EmailEmptyException();
        return modelRepository.isEmailExists(email);
    }

    @Override
    public void lockUserByLogin(@Nullable final String login) throws UserNotFoundException, LoginEmptyException {
        @NotNull final User user = findByLogin(login);
        user.setLocked(true);
    }

    @NotNull
    @Override
    public User removeOne(@Nullable final User model) throws UserNotFoundException {
        if (model == null) throw new UserNotFoundException();
        @NotNull final String userId = model.getId();
        @NotNull final List<Project> projects = projectRepository.findAll(userId);
        for (@NotNull final Project project : projects) {
            taskRepository.removeAllByProjectId(userId, project.getId());
        }
        projectRepository.removeAll(userId);
        try {
            return super.removeOne(model);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
    }

    @NotNull
    @Override
    public User removeByLogin(@Nullable final String login) throws UserNotFoundException, LoginEmptyException {
        @NotNull final User user = findByLogin(login);
        @Nullable final User userRemoved = modelRepository.removeOne(user);
        if (userRemoved == null) throw new UserNotFoundException();
        return userRemoved;
    }

    @NotNull
    @Override
    public User removeByEmail(@Nullable final String email) throws EmailEmptyException, UserNotFoundException {
        @Nullable final User user = findByEmail(email);
        @Nullable final User userRemoved = modelRepository.removeOne(user);
        if (userRemoved == null) throw new UserNotFoundException();
        return userRemoved;
    }

    @NotNull
    @Override
    public User setPassword(
            @Nullable final String id,
            @Nullable final String password
    ) throws AbstractException, NoSuchAlgorithmException {
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @NotNull User user;
        try {
            user = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        return user;
    }

    @Override
    public void unlockUserByLogin(@Nullable final String login) throws UserNotFoundException, LoginEmptyException {
        @NotNull final User user = findByLogin(login);
        user.setLocked(false);
    }

    @NotNull
    @Override
    public User updateUser(
            @Nullable final String id,
            @Nullable final String firstName,
            @Nullable final String middleName,
            @Nullable final String lastName
    ) throws IdEmptyException, UserNotFoundException {
        @NotNull User user;
        try {
            user = findOneById(id);
        } catch (EntityNotFoundException e) {
            throw new UserNotFoundException();
        }
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        return user;
    }

}
