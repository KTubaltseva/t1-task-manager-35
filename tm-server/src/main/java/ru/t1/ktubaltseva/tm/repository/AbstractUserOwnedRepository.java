package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.IUserOwnedRepository;
import ru.t1.ktubaltseva.tm.model.AbstractUserOwnedModel;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public abstract class AbstractUserOwnedRepository<M extends AbstractUserOwnedModel> extends AbstractRepository<M> implements IUserOwnedRepository<M> {

    @NotNull
    public M add(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        model.setUserId(userId);
        return add(model);
    }

    public void clear(@NotNull final String userId) {
        if (userId.isEmpty()) return;
        @NotNull final List<M> userModelsRemoved = findAll(userId);
        models.removeAll(userModelsRemoved);
    }

    public boolean existsById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        return findOneById(userId, id) != null;
    }

    @NotNull
    public List<M> findAll(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .collect(Collectors.toList());
    }

    @NotNull
    public List<M> findAll(
            @NotNull final String userId,
            @NotNull final Comparator<M> comparator
    ) {
        @NotNull final List<M> result = findAll(userId);
        result.sort(comparator);
        return result;
    }

    @Nullable
    public M findOneById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        if (userId.isEmpty()) return null;
        if (id.isEmpty()) return null;
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> id.equals(m.getId()))
                .findFirst()
                .orElse(null);
    }

    @Nullable
    public M findOneByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        if (userId.isEmpty()) return null;
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .skip(index)
                .findFirst()
                .orElse(null);
    }

    public int getSize(@NotNull final String userId) {
        if (userId.isEmpty()) return 0;
        return (int) models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .count();
    }

    public void removeAll(@NotNull final String userId) {
        models.removeAll(findAll(userId));
    }

    @Nullable
    public M removeOne(
            @NotNull final String userId,
            @NotNull final M model
    ) {
        return removeById(userId, model.getId());
    }

    @Nullable
    public M removeById(
            @NotNull final String userId,
            @NotNull final String id
    ) {
        @Nullable final M model = findOneById(userId, id);
        if (model == null) return null;
        return removeOne(model);
    }

    @Nullable
    public M removeByIndex(
            @NotNull final String userId,
            @NotNull final Integer index
    ) {
        @Nullable final M model = findOneByIndex(userId, index);
        if (model == null) return null;
        return removeOne(model);
    }
}
