package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) {
        @NotNull final Task task = new Task(name, description);
        return add(userId, task);
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name
    ) {
        @NotNull final Task task = new Task(name);
        return add(userId, task);
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        if (userId.isEmpty()) return Collections.emptyList();
        if (projectId.isEmpty()) return Collections.emptyList();
        return models
                .stream()
                .filter(m -> userId.equals(m.getUserId()))
                .filter(m -> projectId.equals(m.getProjectId()))
                .collect(Collectors.toList());
    }

    @Override
    public void removeAllByProjectId(
            @NotNull final String userId,
            @NotNull final String projectId
    ) {
        models.removeAll(findAllByProjectId(userId, projectId));
    }

}
