package ru.t1.ktubaltseva.tm.api.repository;

import ru.t1.ktubaltseva.tm.model.Session;

public interface ISessionRepository extends IUserOwnedRepository<Session> {
}
