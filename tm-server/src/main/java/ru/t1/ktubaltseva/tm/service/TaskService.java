package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.enumerated.Sort;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.StatusEmptyException;
import ru.t1.ktubaltseva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public final class TaskService extends AbstractUserOwnedService<Task, ITaskRepository> implements ITaskService {

    public TaskService(@NotNull final ITaskRepository modelRepository) {
        super(modelRepository);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        return modelRepository.create(userId, name, description);
    }

    @NotNull
    @Override
    public Task create(
            @Nullable final String userId,
            @Nullable final String name
    ) throws NameEmptyException, AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        return modelRepository.create(userId, name);
    }

    @NotNull
    @Override
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        if (status == null) throw new StatusEmptyException();
        @NotNull Task task;
        try {
            task = findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public Task changeTaskStatusByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final Status status
    ) throws AbstractException {
        if (status == null) throw new StatusEmptyException();
        @NotNull Task task;
        try {
            task = findOneByIndex(userId, index);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setStatus(status);
        return task;
    }

    @NotNull
    @Override
    public List<Task> findAll(
            @Nullable final String userId,
            @Nullable final Sort sort
    ) throws AuthRequiredException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (sort == null) return findAll(userId);
        return modelRepository.findAll(userId, (Comparator<Task>) sort.getComparator());
    }

    @NotNull
    @Override
    public List<Task> findAllByProjectId(
            @Nullable final String userId,
            @Nullable String projectId
    ) throws AbstractException {
        if (userId == null || userId.isEmpty()) throw new AuthRequiredException();
        if (projectId == null || projectId.isEmpty()) throw new ProjectIdEmptyException();
        return modelRepository.findAllByProjectId(userId, projectId);
    }

    @NotNull
    @Override
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task;
        try {
            task = findOneById(userId, id);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

    @NotNull
    @Override
    public Task updateByIndex(
            @Nullable final String userId,
            @Nullable final Integer index,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull Task task;
        try {
            task = findOneByIndex(userId, index);
        } catch (EntityNotFoundException e) {
            throw new TaskNotFoundException();
        }
        task.setName(name);
        task.setDescription(description);
        return task;
    }

}
