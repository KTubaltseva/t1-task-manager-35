package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Role;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class UserTestData {
    @NotNull
    public final static User USER_1 = new User();

    @NotNull
    public final static User USER_2 = new User();

    @NotNull
    public final static User USER_3 = new User();

    @Nullable
    public final static User NULL_PROJECT = null;

    @Nullable
    public final static User NULL_USER = null;

    @NotNull
    public final static List<User> USER_LIST = Arrays.asList(USER_1, USER_2, USER_3);

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static User NON_EXISTENT_USER = new User();

    @Nullable
    public final static String USER_EMAIL = "TestEmail";

    @Nullable
    public final static String NON_EXISTENT_USER_EMAIL = "NON_EXISTENT_USER_EMAIL";

    @Nullable
    public final static String NULL_EMAIL = null;

    @Nullable
    public final static String USER_PASSWORD = "TestPassword";

    @Nullable
    public final static String NULL_PASSWORD = null;

    @Nullable
    public final static String USER_FIRST_NAME = "USER_FIRST_NAME";

    @Nullable
    public final static String NULL_FIRST_NAME = null;

    @Nullable
    public final static String USER_MIDDLE_NAME = "USER_MIDDLE_NAME";

    @Nullable
    public final static String NULL_MIDDLE_NAME = null;

    @Nullable
    public final static String USER_LAST_NAME = "USER_LAST_NAME";

    @Nullable
    public final static String NULL_LAST_NAME = null;

    @Nullable
    public final static Role USER_ROLE = Role.USUAL;

    @Nullable
    public final static Role NULL_ROLE = null;

    @Nullable
    public final static String USER_LOGIN = "TestLogin";

    @Nullable
    public final static String NON_EXISTENT_USER_LOGIN = "NON_EXISTENT_USER_LOGIN";

    @Nullable
    public final static String NULL_LOGIN = null;

    @NotNull
    public final static User USER_WITH_LOGIN_EMAIL = new User(USER_LOGIN, USER_EMAIL);
}
