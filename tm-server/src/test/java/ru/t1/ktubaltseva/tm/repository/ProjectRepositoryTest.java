package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.ProjectTestData.*;

@Category(UnitCategory.class)
public final class ProjectRepositoryTest {

    @NotNull
    private final IProjectRepository repository = new ProjectRepository();

    @Before
    public void before() {
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void add() {
        Assert.assertThrows(IllegalArgumentException.class, () -> repository.add(NULL_PROJECT));

        @Nullable final Project projectToAdd = PROJECT_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        @Nullable final Project projectAdded = repository.add((projectToAdd));
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(projectToAdd, projectAdded);

        @Nullable final Project projectFindOneById = repository.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd, projectFindOneById);
    }

    @Test
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Project projectToAddByUser = PROJECT_1;
        @Nullable final String projectToAddByUserId = projectToAddByUser.getId();

        @Nullable final Project projectAddedByUser = repository.add(userToAddId, projectToAddByUser);
        Assert.assertNotNull(projectAddedByUser);
        Assert.assertTrue(repository.existsById(projectToAddByUserId));

        @Nullable final Project projectFindOneById = repository.findOneById(projectToAddByUserId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectAddedByUser, projectFindOneById);

        @Nullable final Project projectFindOneByIdByUserIdToAdd = repository.findOneById(userToAddId, projectToAddByUserId);
        Assert.assertNotNull(projectFindOneByIdByUserIdToAdd);
        Assert.assertEquals(projectAddedByUser, projectFindOneByIdByUserIdToAdd);

        @Nullable final Project projectFindOneByIdByUserIdNoAdd = repository.findOneById(userNoAddId, projectToAddByUserId);
        Assert.assertNull(projectFindOneByIdByUserIdNoAdd);
    }

    @Test
    public void addMany() {
        @Nullable final Collection<Project> projectList = repository.add(PROJECT_LIST);
        Assert.assertNotNull(projectList);
        for (@NotNull final Project project : PROJECT_LIST) {
            @Nullable final Project projectFindOneById = repository.findOneById(project.getId());
            Assert.assertEquals(project, projectFindOneById);
        }
    }

    @Test
    public void findOneById() {
        @NotNull final Project projectExists = PROJECT_1;
        repository.add(projectExists);

        @Nullable final Project projectFindOneById = repository.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists, projectFindOneById);

        @Nullable final Project projectFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_PROJECT_ID);
        Assert.assertNull(projectFindOneByIdNonExistent);
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final Project projectExists = PROJECT_1;
        @NotNull final User userExists = USER_1;
        repository.add(userExists.getId(), projectExists);

        Assert.assertNull(repository.findOneById(userExists.getId(), NON_EXISTENT_PROJECT_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, projectExists.getId()));

        @Nullable final Project projectFindOneById = repository.findOneById(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists, projectFindOneById);
    }

    @Test
    public void findAll() {
        @NotNull final Project projectExists = PROJECT_1;

        repository.clear();
        @NotNull final Collection<Project> projectsFindAllEmpty = repository.findAll();
        Assert.assertNotNull(projectsFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), projectsFindAllEmpty);

        repository.add(projectExists);
        @NotNull final Collection<Project> projectsFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
        Assert.assertTrue(projectsFindAllNoEmpty.contains(projectExists));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final Project projectExists = PROJECT_1;
        @NotNull final User userExists = USER_1;

        repository.clear();
        @NotNull final Collection<Project> projectsFindAllByUserRepEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepEmpty);
        Assert.assertEquals(Collections.emptyList(), projectsFindAllByUserRepEmpty);

        repository.add(userExists.getId(), projectExists);
        @NotNull final Collection<Project> projectsFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(projectsFindAllByUserRepNoEmpty);
        Assert.assertTrue(projectsFindAllByUserRepNoEmpty.contains(projectExists));

        @NotNull final Collection<Project> projectsFindAllByNonExistentUser = repository.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(projectsFindAllByNonExistentUser);
        Assert.assertFalse(projectsFindAllByNonExistentUser.contains(projectExists));
        Assert.assertEquals(Collections.emptyList(), projectsFindAllByNonExistentUser);
    }

    @Test
    public void clear() {
        @Nullable final Collection<Project> projectList = repository.add(PROJECT_LIST);

        repository.clear();
        for (@NotNull final Project project : PROJECT_LIST) {
            @Nullable final Project projectFindOneById = repository.findOneById(project.getId());
            Assert.assertNull(projectFindOneById);
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Project> projectByUserToClearList = repository.add(USER_1_PROJECT_LIST);
        @NotNull final Collection<Project> projectByUserNoClearList = repository.add(USER_2_PROJECT_LIST);
        repository.clear(userToClearId);

        for (@NotNull final Project projectByUserToClear : projectByUserToClearList) {
            @Nullable final Project projectFindOneById = repository.findOneById(projectByUserToClear.getId());
            Assert.assertNull(projectFindOneById);
        }
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        for (@NotNull final Project projectByUserNoClear : projectByUserNoClearList) {
            @Nullable final Project projectFindOneById = repository.findOneById(projectByUserNoClear.getId());
            Assert.assertEquals(projectByUserNoClear, projectFindOneById);
        }
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() {
        @Nullable final Project projectToRemove = PROJECT_1;
        repository.add((projectToRemove));

        @Nullable final Project projectRemoved = repository.removeOne(projectToRemove);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove, projectRemoved);

        @Nullable final Project projectFindOneById = repository.findOneById(projectRemoved.getId());
        Assert.assertNull(projectFindOneById);
    }

    @Test
    public void removeOneByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = repository.add((USER_1_PROJECT_1));
        @Nullable final Project projectByUserNoRemove = repository.add((USER_2_PROJECT_1));

        Assert.assertNull(repository.removeOne(NON_EXISTENT_USER_ID, projectByUserToRemove));
        Assert.assertNull(repository.removeOne(userToRemoveId, NON_EXISTENT_PROJECT));

        @Nullable final Project projectRemoved = repository.removeOne(userToRemoveId, projectByUserToRemove);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectRemoved, projectByUserToRemove);
        @Nullable final Project projectRemovedFindOneById = repository.findOneById(projectByUserToRemove.getId());
        Assert.assertNull(projectRemovedFindOneById);

        @Nullable final Project projectNoRemoved = repository.removeOne(userToRemoveId, projectByUserNoRemove);
        Assert.assertNull(projectNoRemoved);
        @Nullable final Project projectNoRemovedFindOneById = repository.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById, projectByUserNoRemove);
    }

    @Test
    public void removeById() {
        @Nullable final Project projectToRemove = PROJECT_1;
        repository.add((projectToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_PROJECT_ID));

        @Nullable final Project projectRemoved = repository.removeById(projectToRemove.getId());
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove, projectRemoved);

        @Nullable final Project projectFindOneById = repository.findOneById(projectRemoved.getId());
        Assert.assertNull(projectFindOneById);
    }

    @Test
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Project projectByUserToRemove = repository.add((USER_1_PROJECT_1));
        @Nullable final Project projectByUserNoRemove = repository.add((USER_2_PROJECT_1));

        Assert.assertNull(repository.removeById(NON_EXISTENT_USER_ID, projectByUserToRemove.getId()));
        Assert.assertNull(repository.removeById(userToRemoveId, NON_EXISTENT_PROJECT_ID));

        @Nullable final Project projectRemoved = repository.removeById(userToRemoveId, projectByUserToRemove.getId());
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectRemoved, projectByUserToRemove);
        @Nullable final Project projectRemovedFindOneById = repository.findOneById(projectByUserToRemove.getId());
        Assert.assertNull(projectRemovedFindOneById);

        @Nullable final Project projectNoRemoved = repository.removeById(userToRemoveId, projectByUserNoRemove.getId());
        Assert.assertNull(projectNoRemoved);
        @Nullable final Project projectNoRemovedFindOneById = repository.findOneById(projectByUserNoRemove.getId());
        Assert.assertNotNull(projectNoRemovedFindOneById);
        Assert.assertEquals(projectNoRemovedFindOneById, projectByUserNoRemove);
    }

    @Test
    public void isExists() {
        @NotNull final Project projectExists = PROJECT_1;
        repository.add(projectExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_PROJECT_ID));
        Assert.assertTrue(repository.existsById(projectExists.getId()));
    }

    @Test
    public void createName() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Project createdProject = repository.create(existentUser.getId(), PROJECT_NAME);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertNotNull(createdProject);
        Assert.assertTrue(repository.existsById(createdProject.getId()));

        @Nullable final Project projectFindOneById = repository.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject, projectFindOneById);
    }

    @Test
    public void createNameDesc() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Project createdProject = repository.create(existentUser.getId(), PROJECT_NAME, PROJECT_DESC);
        Assert.assertEquals(PROJECT_NAME, createdProject.getName());
        Assert.assertEquals(PROJECT_DESC, createdProject.getDescription());
        Assert.assertNotNull(createdProject);
        Assert.assertTrue(repository.existsById(createdProject.getId()));

        @Nullable final Project projectFindOneById = repository.findOneById(createdProject.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdProject, projectFindOneById);
    }

}
