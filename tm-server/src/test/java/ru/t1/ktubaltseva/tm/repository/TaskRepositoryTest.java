package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskRepositoryTest {
    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final IProjectRepository repositoryProject = new ProjectRepository();

    @Before
    public void before() {
    }

    @After
    public void after() {
        repository.clear();
        repositoryProject.clear();
    }

    @Test
    public void add() {
        @Nullable final Task taskToAdd = TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();

        @Nullable final Task taskAdded = repository.add((taskToAdd));
        Assert.assertNotNull(taskAdded);
        Assert.assertEquals(taskToAdd, taskAdded);

        @Nullable final Task taskFindOneById = repository.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd, taskFindOneById);
    }

    @Test
    public void addByUserId() {
        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Task taskToAddByUser = TASK_1;
        @Nullable final String taskToAddByUserId = taskToAddByUser.getId();

        @Nullable final Task taskAddedByUser = repository.add(userToAddId, taskToAddByUser);
        Assert.assertNotNull(taskAddedByUser);
        Assert.assertTrue(repository.existsById(taskToAddByUserId));

        @Nullable final Task taskFindOneById = repository.findOneById(taskToAddByUserId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskAddedByUser, taskFindOneById);

        @Nullable final Task taskFindOneByIdByUserIdToAdd = repository.findOneById(userToAddId, taskToAddByUserId);
        Assert.assertNotNull(taskFindOneByIdByUserIdToAdd);
        Assert.assertEquals(taskAddedByUser, taskFindOneByIdByUserIdToAdd);

        @Nullable final Task taskFindOneByIdByUserIdNoAdd = repository.findOneById(userNoAddId, taskToAddByUserId);
        Assert.assertNull(taskFindOneByIdByUserIdNoAdd);
    }

    @Test
    public void addMany() {
        @Nullable final Collection<Task> taskList = repository.add(TASK_LIST);
        Assert.assertNotNull(taskList);
        for (@NotNull final Task task : TASK_LIST) {
            @Nullable final Task taskFindOneById = repository.findOneById(task.getId());
            Assert.assertEquals(task, taskFindOneById);
        }
    }

    @Test
    public void findOneById() {
        @NotNull final Task taskExists = TASK_1;
        repository.add(taskExists);

        @Nullable final Task taskFindOneById = repository.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists, taskFindOneById);

        @Nullable final Task taskFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_TASK_ID);
        Assert.assertNull(taskFindOneByIdNonExistent);
    }

    @Test
    public void findOneByIdByUserId() {
        @NotNull final Task taskExists = TASK_1;
        @NotNull final User userExists = USER_1;
        repository.add(userExists.getId(), taskExists);

        Assert.assertNull(repository.findOneById(userExists.getId(), NON_EXISTENT_TASK_ID));
        Assert.assertNull(repository.findOneById(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final Task taskFindOneById = repository.findOneById(userExists.getId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists, taskFindOneById);
    }

    @Test
    public void findAll() {
        @NotNull final Task taskExists = TASK_1;

        repository.clear();
        @NotNull final Collection<Task> tasksFindAllEmpty = repository.findAll();
        Assert.assertNotNull(tasksFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllEmpty);

        repository.add(taskExists);
        @NotNull final Collection<Task> tasksFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
        Assert.assertTrue(tasksFindAllNoEmpty.contains(taskExists));
    }

    @Test
    public void findAllByUserId() {
        @NotNull final Task taskExists = TASK_1;
        @NotNull final User userExists = USER_1;

        repository.clear();
        repositoryProject.add(PROJECT_1);
        @NotNull final Collection<Task> tasksFindAllByUserRepEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepEmpty);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserRepEmpty);

        repository.add(userExists.getId(), taskExists);
        @NotNull final Collection<Task> tasksFindAllByUserRepNoEmpty = repository.findAll(userExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);
        Assert.assertTrue(tasksFindAllByUserRepNoEmpty.contains(taskExists));

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = repository.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertFalse(tasksFindAllByNonExistentUser.contains(taskExists));
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    public void findAllByProjectId() {
        @NotNull final Task taskExists = TASK_1;
        @NotNull final User userExists = USER_1;
        @NotNull final Project projectExists = PROJECT_1;

        repository.clear();
        repositoryProject.clear();
        @NotNull final Collection<Task> tasksFindAllByUserRepEmpty = repository.findAllByProjectId(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepEmpty);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserRepEmpty);

        repository.add(userExists.getId(), taskExists);
        repositoryProject.add(userExists.getId(), projectExists);
        @NotNull final Collection<Task> tasksFindAllByUserEmptyProject = repository.findAllByProjectId(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserEmptyProject);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserEmptyProject);
        Assert.assertFalse(tasksFindAllByUserEmptyProject.contains(taskExists));

        taskExists.setProjectId(projectExists.getId());
        @NotNull final Collection<Task> tasksFindAllByUserBindProject = repository.findAllByProjectId(userExists.getId(), projectExists.getId());
        Assert.assertNotNull(tasksFindAllByUserBindProject);
        Assert.assertTrue(tasksFindAllByUserBindProject.contains(taskExists));

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = repository.findAllByProjectId(NON_EXISTENT_USER_ID, projectExists.getId());
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertFalse(tasksFindAllByNonExistentUser.contains(taskExists));
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);

        @NotNull final Collection<Task> tasksFindAllByNonExistentProject = repository.findAllByProjectId(userExists.getId(), NON_EXISTENT_PROJECT_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentProject);
        Assert.assertFalse(tasksFindAllByNonExistentProject.contains(taskExists));
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentProject);
    }

    @Test
    public void clear() {
        @Nullable final Collection<Task> taskList = repository.add(TASK_LIST);

        repository.clear();
        for (@NotNull final Task task : TASK_LIST) {
            @Nullable final Task taskFindOneById = repository.findOneById(task.getId());
            Assert.assertNull(taskFindOneById);
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void clearByUserId() {
        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Task> taskByUserToClearList = repository.add(USER_1_TASK_LIST);
        @NotNull final Collection<Task> taskByUserNoClearList = repository.add(USER_2_TASK_LIST);
        repository.clear(userToClearId);

        for (@NotNull final Task taskByUserToClear : taskByUserToClearList) {
            @Nullable final Task taskFindOneById = repository.findOneById(taskByUserToClear.getId());
            Assert.assertNull(taskFindOneById);
        }
        Assert.assertEquals(0, repository.findAll(userToClearId).size());

        for (@NotNull final Task taskByUserNoClear : taskByUserNoClearList) {
            @Nullable final Task taskFindOneById = repository.findOneById(taskByUserNoClear.getId());
            Assert.assertEquals(taskByUserNoClear, taskFindOneById);
        }
        Assert.assertNotEquals(0, repository.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() {
        @Nullable final Task taskToRemove = TASK_1;
        repository.add((taskToRemove));

        @Nullable final Task taskRemoved = repository.removeOne(taskToRemove);
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskToRemove, taskRemoved);

        @Nullable final Task taskFindOneById = repository.findOneById(taskRemoved.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    public void removeOneByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = repository.add((USER_1_TASK_1));
        @Nullable final Task taskByUserNoRemove = repository.add((USER_2_TASK_1));

        Assert.assertNull(repository.removeOne(NON_EXISTENT_TASK_ID, taskByUserToRemove));
        Assert.assertNull(repository.removeOne(userToRemoveId, NON_EXISTENT_TASK));

        @Nullable final Task taskRemoved = repository.removeOne(userToRemoveId, taskByUserToRemove);
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskRemoved, taskByUserToRemove);
        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemoved = repository.removeOne(userToRemoveId, taskByUserNoRemove);
        Assert.assertNull(taskNoRemoved);
        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById, taskByUserNoRemove);
    }

    @Test
    public void removeById() {
        @Nullable final Task taskToRemove = TASK_1;
        repository.add((taskToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_TASK_ID));

        @Nullable final Task taskRemoved = repository.removeById(taskToRemove.getId());
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskToRemove, taskRemoved);

        @Nullable final Task taskFindOneById = repository.findOneById(taskRemoved.getId());
        Assert.assertNull(taskFindOneById);
    }

    @Test
    public void removeByIdByUserId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = repository.add((USER_1_TASK_1));
        @Nullable final Task taskByUserNoRemove = repository.add((USER_2_TASK_1));

        Assert.assertNull(repository.removeById(NON_EXISTENT_USER_ID, taskByUserToRemove.getId()));
        Assert.assertNull(repository.removeById(userToRemoveId, NON_EXISTENT_TASK_ID));

        @Nullable final Task taskRemoved = repository.removeById(userToRemoveId, taskByUserToRemove.getId());
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskRemoved, taskByUserToRemove);
        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByUserToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemoved = repository.removeById(userToRemoveId, taskByUserNoRemove.getId());
        Assert.assertNull(taskNoRemoved);
        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById, taskByUserNoRemove);
    }

    @Test
    public void removeAllByIdByProjectId() {
        @NotNull final User userToRemove = USER_1;
        @NotNull final Project projectToRemove = USER_1_PROJECT_1;
        @NotNull final Project projectNoRemove = USER_1_PROJECT_2;
        @Nullable final Task taskByProjectToRemove = USER_1_TASK_1;
        @Nullable final Task taskByProjectNoRemove = USER_1_TASK_2;

        repositoryProject.add(projectToRemove);
        repositoryProject.add(projectNoRemove);
        repository.add(taskByProjectToRemove);
        repository.add(taskByProjectNoRemove);
        taskByProjectToRemove.setProjectId(projectToRemove.getId());
        taskByProjectNoRemove.setProjectId(projectNoRemove.getId());
        repository.removeAllByProjectId(userToRemove.getId(), projectToRemove.getId());

        @Nullable final Task taskRemovedFindOneById = repository.findOneById(taskByProjectToRemove.getId());
        Assert.assertNull(taskRemovedFindOneById);

        @Nullable final Task taskNoRemovedFindOneById = repository.findOneById(taskByProjectNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById, taskByProjectNoRemove);
    }

    @Test
    public void isExists() {
        @NotNull final Task taskExists = TASK_1;
        repository.add(taskExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_TASK_ID));
        Assert.assertTrue(repository.existsById(taskExists.getId()));
    }

    @Test
    public void createName() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Task createdTask = repository.create(existentUser.getId(), TASK_NAME);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertNotNull(createdTask);
        Assert.assertTrue(repository.existsById(createdTask.getId()));

        @Nullable final Task taskFindOneById = repository.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask, taskFindOneById);
    }

    @Test
    public void createNameDesc() {
        @NotNull final User existentUser = USER_1;

        @NotNull final Task createdTask = repository.create(existentUser.getId(), TASK_NAME, TASK_DESC);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertEquals(TASK_DESC, createdTask.getDescription());
        Assert.assertNotNull(createdTask);
        Assert.assertTrue(repository.existsById(createdTask.getId()));

        @Nullable final Task taskFindOneById = repository.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask, taskFindOneById);
    }

}
