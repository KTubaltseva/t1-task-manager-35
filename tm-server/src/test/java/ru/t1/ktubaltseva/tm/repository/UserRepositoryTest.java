package ru.t1.ktubaltseva.tm.repository;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @Before
    public void before() {
    }

    @After
    public void after() {
        repository.clear();
    }

    @Test
    public void add() {
        @Nullable final User userToAdd = USER_1;
        @Nullable final String userToAddId = userToAdd.getId();

        @Nullable final User userAdded = repository.add((userToAdd));
        Assert.assertNotNull(userAdded);
        Assert.assertEquals(userToAdd, userAdded);

        @Nullable final User userFindOneById = repository.findOneById(userToAddId);
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userToAdd, userFindOneById);
    }

    @Test
    public void addMany() {
        @Nullable final Collection<User> userList = repository.add(USER_LIST);
        Assert.assertNotNull(userList);
        for (@NotNull final User user : USER_LIST) {
            @Nullable final User userFindOneById = repository.findOneById(user.getId());
            Assert.assertEquals(user, userFindOneById);
        }
    }

    @Test
    public void findOneById() {
        @NotNull final User userExists = USER_1;
        repository.add(userExists);

        @Nullable final User userFindOneById = repository.findOneById(userExists.getId());
        Assert.assertNotNull(userFindOneById);
        Assert.assertEquals(userExists, userFindOneById);

        @Nullable final User userFindOneByIdNonExistent = repository.findOneById(NON_EXISTENT_USER_ID);
        Assert.assertNull(userFindOneByIdNonExistent);
    }

    @Test
    public void findByEmail() {
        @NotNull final User userWithEmail = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithEmail);

        @Nullable final User userFindByNotExistentEmail = repository.findByEmail(NON_EXISTENT_USER_EMAIL);
        Assert.assertNull(userFindByNotExistentEmail);

        @Nullable final User userFindByEmail = repository.findByEmail(USER_EMAIL);
        Assert.assertNotNull(userFindByEmail);
        Assert.assertEquals(userWithEmail, userFindByEmail);
    }

    @Test
    public void findByLogin() {
        @NotNull final User userWithLogin = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithLogin);

        @Nullable final User userFindByNotExistentLogin = repository.findByLogin(NON_EXISTENT_USER_LOGIN);
        Assert.assertNull(userFindByNotExistentLogin);

        @Nullable final User userFindByLogin = repository.findByLogin(USER_LOGIN);
        Assert.assertNotNull(userFindByLogin);
        Assert.assertEquals(userWithLogin, userFindByLogin);
    }

    @Test
    public void findAll() {
        @NotNull final User userExists = USER_1;

        repository.clear();
        @NotNull final Collection<User> usersFindAllEmpty = repository.findAll();
        Assert.assertNotNull(usersFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), usersFindAllEmpty);

        repository.add(userExists);
        @NotNull final Collection<User> usersFindAllNoEmpty = repository.findAll();
        Assert.assertNotNull(usersFindAllNoEmpty);
        Assert.assertTrue(usersFindAllNoEmpty.contains(userExists));
    }

    @Test
    public void clear() {
        @Nullable final Collection<User> userList = repository.add(USER_LIST);

        repository.clear();
        for (@NotNull final User user : USER_LIST) {
            @Nullable final User userFindOneById = repository.findOneById(user.getId());
            Assert.assertNull(userFindOneById);
        }
        Assert.assertEquals(0, repository.getSize());
    }

    @Test
    public void removeOne() {
        @Nullable final User userToRemove = USER_1;
        repository.add((userToRemove));

        @Nullable final User userRemoved = repository.removeOne(userToRemove);
        Assert.assertNotNull(userRemoved);
        Assert.assertEquals(userToRemove, userRemoved);

        @Nullable final User userFindOneById = repository.findOneById(userRemoved.getId());
        Assert.assertNull(userFindOneById);
    }

    @Test
    public void removeById() {
        @Nullable final User userToRemove = USER_1;
        repository.add((userToRemove));

        Assert.assertNull(repository.removeById(NON_EXISTENT_USER_ID));

        @Nullable final User userRemoved = repository.removeById(userToRemove.getId());
        Assert.assertNotNull(userRemoved);
        Assert.assertEquals(userToRemove, userRemoved);

        @Nullable final User userFindOneById = repository.findOneById(userRemoved.getId());
        Assert.assertNull(userFindOneById);
    }

    @Test
    public void isEmailExists() {
        @NotNull final User userWithEmail = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithEmail);

        Assert.assertFalse(repository.isEmailExists(NON_EXISTENT_USER_EMAIL));
        Assert.assertTrue(repository.isEmailExists(USER_EMAIL));
    }

    @Test
    public void isLoginExists() {
        @NotNull final User userWithLogin = USER_WITH_LOGIN_EMAIL;
        repository.add(userWithLogin);

        Assert.assertFalse(repository.isLoginExists(NON_EXISTENT_USER_LOGIN));
        Assert.assertTrue(repository.isLoginExists(USER_LOGIN));
    }

    @Test
    public void isExists() {
        @NotNull final User userExists = USER_1;
        repository.add(userExists);

        Assert.assertFalse(repository.existsById(NON_EXISTENT_USER_ID));
        Assert.assertTrue(repository.existsById(userExists.getId()));
    }

}
