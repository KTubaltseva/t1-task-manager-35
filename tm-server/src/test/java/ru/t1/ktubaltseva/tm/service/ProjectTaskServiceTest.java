package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.IProjectTaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.ProjectNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.ProjectIdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.TaskIdEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;

import static ru.t1.ktubaltseva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class ProjectTaskServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectTaskService service = new ProjectTaskService(projectRepository, taskRepository);

    @Before
    public void before() {
    }

    @After
    public void after() {
        taskRepository.clear();
        projectRepository.clear();
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        @Nullable final User user = USER_1;
        @Nullable final Task task = USER_1_TASK_1;
        @Nullable final Project project = USER_1_PROJECT_1;
        projectRepository.add(project);
        taskRepository.add(task);

        Assert.assertThrows(AuthRequiredException.class, () -> service.bindTaskToProject(NULL_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(NON_EXISTENT_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), NULL_PROJECT_ID, task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.bindTaskToProject(user.getId(), NON_EXISTENT_PROJECT_ID, task.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.bindTaskToProject(user.getId(), project.getId(), NON_EXISTENT_TASK_ID));

        @Nullable final Task taskBinded = service.bindTaskToProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(taskBinded);
        Assert.assertEquals(task, taskBinded);
        Assert.assertEquals(project.getId(), task.getProjectId());
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        @Nullable final User user = USER_1;
        @Nullable final Task task = USER_1_TASK_1;
        @Nullable final Project project = USER_1_PROJECT_1;
        projectRepository.add(project);
        taskRepository.add(task);
        task.setProjectId(project.getId());

        Assert.assertThrows(AuthRequiredException.class, () -> service.unbindTaskFromProject(NULL_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(NON_EXISTENT_USER_ID, project.getId(), task.getId()));
        Assert.assertThrows(ProjectIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), NULL_PROJECT_ID, task.getId()));
        Assert.assertThrows(ProjectNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), NON_EXISTENT_PROJECT_ID, task.getId()));
        Assert.assertThrows(TaskIdEmptyException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), NULL_TASK_ID));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.unbindTaskFromProject(user.getId(), project.getId(), NON_EXISTENT_TASK_ID));

        @Nullable final Task taskUnbind = service.unbindTaskFromProject(user.getId(), project.getId(), task.getId());
        Assert.assertNotNull(taskUnbind);
        Assert.assertEquals(task, taskUnbind);
        Assert.assertEquals(NULL_PROJECT_ID, task.getProjectId());
    }

}
