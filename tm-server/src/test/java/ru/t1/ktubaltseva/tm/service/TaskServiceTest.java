package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.service.ITaskService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.auth.AuthRequiredException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.TaskNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.DescriptionEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.IdEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.NameEmptyException;
import ru.t1.ktubaltseva.tm.exception.field.StatusEmptyException;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.Task;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;

import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.TaskTestData.*;

@Category(UnitCategory.class)
public final class TaskServiceTest {

    @NotNull
    private final ITaskRepository repository = new TaskRepository();

    @NotNull
    private final ITaskService service = new TaskService(repository);

    @Before
    public void before() {
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_TASK));

        @Nullable final Task taskToAdd = TASK_1;
        @Nullable final String taskToAddId = taskToAdd.getId();

        @Nullable final Task taskAdded = service.add(taskToAdd);
        Assert.assertNotNull(taskAdded);
        Assert.assertEquals(taskToAdd, taskAdded);

        @Nullable final Task taskFindOneById = service.findOneById(taskToAddId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskToAdd, taskFindOneById);
    }

    @Test
    public void addByUserId() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(USER_1.getId(), NULL_TASK));
        Assert.assertThrows(AuthRequiredException.class, () -> service.add(NULL_USER_ID, TASK_1));

        @Nullable final String userToAddId = USER_1.getId();
        @Nullable final String userNoAddId = USER_2.getId();
        @Nullable final Task taskToAddByUser = TASK_1;
        @Nullable final String taskToAddByUserId = taskToAddByUser.getId();

        @Nullable final Task taskAddedByUser = service.add(userToAddId, taskToAddByUser);
        Assert.assertNotNull(taskAddedByUser);
        Assert.assertTrue(service.existsById(taskToAddByUserId));

        @Nullable final Task taskFindOneById = service.findOneById(taskToAddByUserId);
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskAddedByUser, taskFindOneById);

        @Nullable final Task taskFindOneByIdByUserIdToAdd = service.findOneById(userToAddId, taskToAddByUserId);
        Assert.assertNotNull(taskFindOneByIdByUserIdToAdd);
        Assert.assertEquals(taskAddedByUser, taskFindOneByIdByUserIdToAdd);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userNoAddId, taskToAddByUserId));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userToAddId, NON_EXISTENT_TASK_ID));
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<Task> taskList = service.add(TASK_LIST);
        Assert.assertNotNull(taskList);
        for (@NotNull final Task task : TASK_LIST) {
            @Nullable final Task taskFindOneById = service.findOneById(task.getId());
            Assert.assertEquals(task, taskFindOneById);
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final Task taskExists = TASK_1;
        service.add(taskExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_TASK_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_TASK_ID));

        @Nullable final Task taskFindOneById = service.findOneById(taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists, taskFindOneById);
    }

    @Test
    public void findOneByIdByUserId() throws AbstractException {
        @NotNull final Task taskExists = TASK_1;
        @NotNull final User userExists = USER_1;
        service.add(userExists.getId(), taskExists);

        Assert.assertThrows(AuthRequiredException.class, () -> service.findOneById(NULL_USER_ID, taskExists.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(userExists.getId(), NULL_TASK_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(userExists.getId(), NON_EXISTENT_TASK_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID, taskExists.getId()));

        @Nullable final Task taskFindOneById = service.findOneById(userExists.getId(), taskExists.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(taskExists, taskFindOneById);
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final Task taskExists = TASK_1;

        service.clear();
        @NotNull final Collection<Task> tasksFindAllEmpty = service.findAll();
        Assert.assertNotNull(tasksFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllEmpty);

        service.add(taskExists);
        @NotNull final Collection<Task> tasksFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(tasksFindAllNoEmpty);
        Assert.assertTrue(tasksFindAllNoEmpty.contains(taskExists));
    }

    @Test
    public void findAllByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.findAll(NULL_USER_ID));

        @NotNull final Task taskExists = TASK_1;
        @NotNull final User userExists = USER_1;

        service.clear();
        @NotNull final Collection<Task> tasksFindAllByUserRepEmpty = service.findAll(userExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepEmpty);
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByUserRepEmpty);

        service.add(userExists.getId(), taskExists);
        @NotNull final Collection<Task> tasksFindAllByUserRepNoEmpty = service.findAll(userExists.getId());
        Assert.assertNotNull(tasksFindAllByUserRepNoEmpty);
        Assert.assertTrue(tasksFindAllByUserRepNoEmpty.contains(taskExists));

        @NotNull final Collection<Task> tasksFindAllByNonExistentUser = service.findAll(NON_EXISTENT_USER_ID);
        Assert.assertNotNull(tasksFindAllByNonExistentUser);
        Assert.assertFalse(tasksFindAllByNonExistentUser.contains(taskExists));
        Assert.assertEquals(Collections.emptyList(), tasksFindAllByNonExistentUser);
    }

    @Test
    public void clear() {
        @Nullable final Collection<Task> taskList = service.add(TASK_LIST);

        service.clear();
        for (@NotNull final Task task : TASK_LIST) {
            Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(task.getId()));
        }
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    public void clearByUserId() throws AbstractException {
        Assert.assertThrows(AuthRequiredException.class, () -> service.clear(NULL_USER_ID));

        @NotNull final User userToClear = USER_1;
        @NotNull final User userNoClear = USER_2;
        @NotNull final String userToClearId = userToClear.getId();
        @NotNull final String userNoClearId = userNoClear.getId();
        @NotNull final Collection<Task> taskByUserToClearList = service.add(USER_1_TASK_LIST);
        @NotNull final Collection<Task> taskByUserNoClearList = service.add(USER_2_TASK_LIST);
        service.clear(userToClearId);

        for (@NotNull final Task taskByUserToClear : taskByUserToClearList) {
            Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(taskByUserToClear.getId()));
        }
        Assert.assertEquals(0, service.findAll(userToClearId).size());

        for (@NotNull final Task taskByUserNoClear : taskByUserNoClearList) {
            @Nullable final Task taskFindOneById = service.findOneById(taskByUserNoClear.getId());
            Assert.assertEquals(taskByUserNoClear, taskFindOneById);
        }
        Assert.assertNotEquals(0, service.findAll(userNoClearId).size());
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final Task taskToRemove = TASK_1;
        service.add((taskToRemove));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(NULL_TASK));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(NON_EXISTENT_TASK));

        @Nullable final Task taskRemoved = service.removeOne(taskToRemove);
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskToRemove, taskRemoved);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(taskRemoved.getId()));
    }

    @Test
    public void removeOneByUserId() throws AbstractException {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = service.add((USER_1_TASK_1));
        @Nullable final Task taskByUserNoRemove = service.add((USER_2_TASK_1));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(NON_EXISTENT_USER_ID, taskByUserToRemove));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeOne(NULL_USER_ID, taskByUserToRemove));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(userToRemoveId, NULL_TASK));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(userToRemoveId, NON_EXISTENT_TASK));

        @Nullable final Task taskRemoved = service.removeOne(userToRemoveId, taskByUserToRemove);
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskRemoved, taskByUserToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(taskByUserToRemove.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeOne(userToRemoveId, taskByUserNoRemove));

        @Nullable final Task taskNoRemovedFindOneById = service.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById, taskByUserNoRemove);
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final Task taskToRemove = TASK_1;
        service.add((taskToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_TASK_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_TASK_ID));

        @Nullable final Task taskRemoved = service.removeById(taskToRemove.getId());
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskToRemove, taskRemoved);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(taskRemoved.getId()));
    }

    @Test
    public void removeByIdByUserId() throws AbstractException {
        @NotNull final User userToRemove = USER_1;
        @NotNull final String userToRemoveId = userToRemove.getId();
        @Nullable final Task taskByUserToRemove = service.add((USER_1_TASK_1));
        @Nullable final Task taskByUserNoRemove = service.add((USER_2_TASK_1));

        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID, taskByUserToRemove.getId()));
        Assert.assertThrows(AuthRequiredException.class, () -> service.removeById(NULL_USER_ID, taskByUserToRemove.getId()));
        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(userToRemoveId, NULL_TASK_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(userToRemoveId, NON_EXISTENT_TASK_ID));

        @Nullable final Task taskRemoved = service.removeById(userToRemoveId, taskByUserToRemove.getId());
        Assert.assertNotNull(taskRemoved);
        Assert.assertEquals(taskRemoved, taskByUserToRemove);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(taskByUserToRemove.getId()));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(userToRemoveId, taskByUserNoRemove.getId()));

        @Nullable final Task taskNoRemovedFindOneById = service.findOneById(taskByUserNoRemove.getId());
        Assert.assertNotNull(taskNoRemovedFindOneById);
        Assert.assertEquals(taskNoRemovedFindOneById, taskByUserNoRemove);
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final Task taskExists = TASK_1;
        service.add(taskExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_TASK_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_TASK_ID));
        Assert.assertTrue(service.existsById(taskExists.getId()));
    }

    @Test
    public void createName() throws AbstractException {
        @NotNull final User existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, TASK_NAME));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME));

        @NotNull final Task createdTask = service.create(existentUser.getId(), TASK_NAME);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertTrue(service.existsById(createdTask.getId()));

        @Nullable final Task taskFindOneById = service.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask, taskFindOneById);
    }

    @Test
    public void createNameDesc() throws AbstractException {
        @NotNull final User existentUser = USER_1;
        Assert.assertThrows(AuthRequiredException.class, () -> service.create(NULL_USER_ID, TASK_NAME, TASK_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.create(existentUser.getId(), NULL_NAME, TASK_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.create(existentUser.getId(), TASK_NAME, NULL_DESC));

        @NotNull final Task createdTask = service.create(existentUser.getId(), TASK_NAME, TASK_DESC);
        Assert.assertNotNull(createdTask);
        Assert.assertEquals(TASK_NAME, createdTask.getName());
        Assert.assertEquals(TASK_DESC, createdTask.getDescription());
        Assert.assertTrue(service.existsById(createdTask.getId()));

        @Nullable final Task taskFindOneById = service.findOneById(createdTask.getId());
        Assert.assertNotNull(taskFindOneById);
        Assert.assertEquals(createdTask, taskFindOneById);
    }

    @Test
    public void changeTaskStatusById() throws AbstractException {
        @NotNull final User userToUpdate = USER_1;
        @NotNull final User userNoUpdate = USER_2;
        @Nullable final Task taskToUpdate = service.add((USER_1_TASK_1));
        @Nullable final Task taskNoUpdate = service.add((USER_1_TASK_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.changeTaskStatusById(NULL_USER_ID, taskToUpdate.getId(), TASK_STATUS));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.changeTaskStatusById(NON_EXISTENT_USER_ID, taskToUpdate.getId(), TASK_STATUS));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.changeTaskStatusById(userNoUpdate.getId(), taskToUpdate.getId(), TASK_STATUS));
        Assert.assertThrows(IdEmptyException.class, () -> service.changeTaskStatusById(userToUpdate.getId(), NULL_TASK_ID, TASK_STATUS));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.changeTaskStatusById(userToUpdate.getId(), NON_EXISTENT_TASK_ID, TASK_STATUS));
        Assert.assertThrows(StatusEmptyException.class, () -> service.changeTaskStatusById(userToUpdate.getId(), taskToUpdate.getId(), NULL_STATUS));

        @Nullable final Task taskUpdated = service.changeTaskStatusById(userToUpdate.getId(), taskToUpdate.getId(), TASK_STATUS);
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(taskUpdated, taskToUpdate);
        Assert.assertEquals(TASK_STATUS, taskUpdated.getStatus());

        @Nullable final Task taskFindOneByIdToUpdate = service.findOneById(taskToUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdToUpdate);
        Assert.assertEquals(taskFindOneByIdToUpdate, taskToUpdate);
        Assert.assertEquals(TASK_STATUS, taskFindOneByIdToUpdate.getStatus());

        @Nullable final Task taskFindOneByIdNoUpdate = service.findOneById(taskNoUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdNoUpdate);
        Assert.assertEquals(taskFindOneByIdNoUpdate, taskNoUpdate);
        Assert.assertNotEquals(TASK_STATUS, taskFindOneByIdNoUpdate.getStatus());
    }

    @Test
    public void updateTaskById() throws AbstractException {
        @NotNull final User userToUpdate = USER_1;
        @NotNull final User userNoUpdate = USER_2;
        @Nullable final Task taskToUpdate = service.add((USER_1_TASK_1));
        @Nullable final Task taskNoUpdate = service.add((USER_1_TASK_2));

        Assert.assertThrows(AuthRequiredException.class, () -> service.updateById(NULL_USER_ID, taskToUpdate.getId(), TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.updateById(NON_EXISTENT_USER_ID, taskToUpdate.getId(), TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.updateById(userNoUpdate.getId(), taskToUpdate.getId(), TASK_NAME, TASK_DESC));
        Assert.assertThrows(IdEmptyException.class, () -> service.updateById(userToUpdate.getId(), NULL_TASK_ID, TASK_NAME, TASK_DESC));
        Assert.assertThrows(TaskNotFoundException.class, () -> service.updateById(userToUpdate.getId(), NON_EXISTENT_TASK_ID, TASK_NAME, TASK_DESC));
        Assert.assertThrows(NameEmptyException.class, () -> service.updateById(userToUpdate.getId(), taskToUpdate.getId(), NULL_NAME, TASK_DESC));
        Assert.assertThrows(DescriptionEmptyException.class, () -> service.updateById(userToUpdate.getId(), taskToUpdate.getId(), TASK_NAME, NULL_DESC));

        @Nullable final Task taskUpdated = service.updateById(userToUpdate.getId(), taskToUpdate.getId(), TASK_NAME, TASK_DESC);
        Assert.assertNotNull(taskUpdated);
        Assert.assertEquals(taskUpdated, taskToUpdate);
        Assert.assertEquals(TASK_NAME, taskUpdated.getName());
        Assert.assertEquals(TASK_DESC, taskUpdated.getDescription());

        @Nullable final Task taskFindOneByIdToUpdate = service.findOneById(taskToUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdToUpdate);
        Assert.assertEquals(taskFindOneByIdToUpdate, taskToUpdate);
        Assert.assertEquals(TASK_NAME, taskFindOneByIdToUpdate.getName());
        Assert.assertEquals(TASK_DESC, taskFindOneByIdToUpdate.getDescription());

        @Nullable final Task taskFindOneByIdNoUpdate = service.findOneById(taskNoUpdate.getId());
        Assert.assertNotNull(taskFindOneByIdNoUpdate);
        Assert.assertEquals(taskFindOneByIdNoUpdate, taskNoUpdate);
        Assert.assertNotEquals(TASK_NAME, taskFindOneByIdNoUpdate.getName());
        Assert.assertNotEquals(TASK_DESC, taskFindOneByIdNoUpdate.getDescription());
    }

}
