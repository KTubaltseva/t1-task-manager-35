package ru.t1.ktubaltseva.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.ktubaltseva.tm.api.repository.IProjectRepository;
import ru.t1.ktubaltseva.tm.api.repository.ITaskRepository;
import ru.t1.ktubaltseva.tm.api.repository.IUserRepository;
import ru.t1.ktubaltseva.tm.api.service.ILoggerService;
import ru.t1.ktubaltseva.tm.api.service.IPropertyService;
import ru.t1.ktubaltseva.tm.api.service.IUserService;
import ru.t1.ktubaltseva.tm.exception.AbstractException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.EntityNotFoundException;
import ru.t1.ktubaltseva.tm.exception.entity.entityNotFound.UserNotFoundException;
import ru.t1.ktubaltseva.tm.exception.field.*;
import ru.t1.ktubaltseva.tm.marker.UnitCategory;
import ru.t1.ktubaltseva.tm.model.User;
import ru.t1.ktubaltseva.tm.repository.ProjectRepository;
import ru.t1.ktubaltseva.tm.repository.TaskRepository;
import ru.t1.ktubaltseva.tm.repository.UserRepository;
import ru.t1.ktubaltseva.tm.util.HashUtil;

import java.security.NoSuchAlgorithmException;
import java.util.Collection;
import java.util.Collections;

import static ru.t1.ktubaltseva.tm.constant.UserTestData.*;

@Category(UnitCategory.class)
public final class UserServiceTest {

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IPropertyService propertyService = new PropertyService(loggerService);

    @NotNull
    private final IUserRepository repository = new UserRepository();

    @NotNull
    private final IUserService service = new UserService(repository, taskRepository, projectRepository, propertyService);

    @Before
    public void before() {
    }

    @After
    public void after() {
        service.clear();
    }

    @Test
    public void add() throws AbstractException {
        Assert.assertThrows(EntityNotFoundException.class, () -> service.add(NULL_USER));

        @Nullable final User projectToAdd = USER_1;
        @Nullable final String projectToAddId = projectToAdd.getId();

        @Nullable final User projectAdded = service.add(projectToAdd);
        Assert.assertNotNull(projectAdded);
        Assert.assertEquals(projectToAdd, projectAdded);

        @Nullable final User projectFindOneById = service.findOneById(projectToAddId);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectToAdd, projectFindOneById);
    }

    @Test
    public void addMany() throws AbstractException {
        @Nullable final Collection<User> projectList = service.add(USER_LIST);
        Assert.assertNotNull(projectList);
        for (@NotNull final User project : USER_LIST) {
            @Nullable final User projectFindOneById = service.findOneById(project.getId());
            Assert.assertEquals(project, projectFindOneById);
        }
    }

    @Test
    public void findOneById() throws AbstractException {
        @NotNull final User projectExists = USER_1;
        service.add(projectExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.findOneById(NULL_USER_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(NON_EXISTENT_USER_ID));

        @Nullable final User projectFindOneById = service.findOneById(projectExists.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists, projectFindOneById);
    }

    @Test
    public void findByLogin() throws AbstractException {
        @NotNull final User projectExists = USER_1;
        projectExists.setLogin(USER_LOGIN);
        service.add(projectExists);

        Assert.assertThrows(LoginEmptyException.class, () -> service.findByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.findByLogin(NON_EXISTENT_USER_LOGIN));

        @Nullable final User projectFindOneById = service.findByLogin(USER_LOGIN);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists, projectFindOneById);
    }

    @Test
    public void findByEmail() throws AbstractException {
        @NotNull final User projectExists = USER_1;
        projectExists.setEmail(USER_EMAIL);
        service.add(projectExists);

        Assert.assertThrows(EmailEmptyException.class, () -> service.findByEmail(NULL_EMAIL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.findByEmail(NON_EXISTENT_USER_EMAIL));

        @Nullable final User projectFindOneById = service.findByEmail(USER_EMAIL);
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(projectExists, projectFindOneById);
    }

    @Test
    public void findAll() throws AbstractException {
        @NotNull final User projectExists = USER_1;

        service.clear();
        @NotNull final Collection<User> projectsFindAllEmpty = service.findAll();
        Assert.assertNotNull(projectsFindAllEmpty);
        Assert.assertEquals(Collections.emptyList(), projectsFindAllEmpty);

        service.add(projectExists);
        @NotNull final Collection<User> projectsFindAllNoEmpty = service.findAll();
        Assert.assertNotNull(projectsFindAllNoEmpty);
        Assert.assertTrue(projectsFindAllNoEmpty.contains(projectExists));
    }

    @Test
    public void clear() {
        @Nullable final Collection<User> projectList = service.add(USER_LIST);

        service.clear();
        for (@NotNull final User project : USER_LIST) {
            Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(project.getId()));
        }
        Assert.assertEquals(0, service.findAll().size());
    }

    @Test
    public void createLoginPassword() throws AbstractException, NoSuchAlgorithmException {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(NULL_LOGIN, USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_LOGIN, NULL_PASSWORD));

        @NotNull final User createdUser = service.create(USER_LOGIN, USER_PASSWORD);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(USER_LOGIN, createdUser.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), createdUser.getPasswordHash());
        Assert.assertTrue(service.existsById(createdUser.getId()));

        @Nullable final User projectFindOneById = service.findOneById(createdUser.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdUser, projectFindOneById);
    }

    @Test
    public void createLoginPasswordEmail() throws AbstractException, NoSuchAlgorithmException {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(NULL_LOGIN, USER_PASSWORD, USER_EMAIL));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_LOGIN, NULL_PASSWORD, USER_EMAIL));
        Assert.assertThrows(EmailEmptyException.class, () -> service.create(USER_LOGIN, USER_PASSWORD, NULL_EMAIL));

        @NotNull final User createdUser = service.create(USER_LOGIN, USER_PASSWORD, USER_EMAIL);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(USER_LOGIN, createdUser.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), createdUser.getPasswordHash());
        Assert.assertEquals(USER_EMAIL, createdUser.getEmail());
        Assert.assertTrue(service.existsById(createdUser.getId()));

        @Nullable final User projectFindOneById = service.findOneById(createdUser.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdUser, projectFindOneById);
    }

    @Test
    public void createLoginPasswordRole() throws AbstractException, NoSuchAlgorithmException {
        Assert.assertThrows(LoginEmptyException.class, () -> service.create(NULL_LOGIN, USER_PASSWORD, USER_ROLE));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.create(USER_LOGIN, NULL_PASSWORD, USER_ROLE));
        Assert.assertThrows(RoleEmptyException.class, () -> service.create(USER_LOGIN, USER_PASSWORD, NULL_ROLE));

        @NotNull final User createdUser = service.create(USER_LOGIN, USER_PASSWORD, USER_ROLE);
        Assert.assertNotNull(createdUser);
        Assert.assertEquals(USER_LOGIN, createdUser.getLogin());
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), createdUser.getPasswordHash());
        Assert.assertEquals(USER_ROLE, createdUser.getRole());
        Assert.assertTrue(service.existsById(createdUser.getId()));

        @Nullable final User projectFindOneById = service.findOneById(createdUser.getId());
        Assert.assertNotNull(projectFindOneById);
        Assert.assertEquals(createdUser, projectFindOneById);
    }

    @Test
    public void removeOne() throws AbstractException {
        @Nullable final User projectToRemove = USER_1;
        service.add((projectToRemove));

        Assert.assertThrows(UserNotFoundException.class, () -> service.removeOne(NULL_USER));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeOne(NON_EXISTENT_USER));

        @Nullable final User projectRemoved = service.removeOne(projectToRemove);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove, projectRemoved);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectRemoved.getId()));
    }

    @Test
    public void removeById() throws AbstractException {
        @Nullable final User projectToRemove = USER_1;
        service.add((projectToRemove));

        Assert.assertThrows(IdEmptyException.class, () -> service.removeById(NULL_USER_ID));
        Assert.assertThrows(EntityNotFoundException.class, () -> service.removeById(NON_EXISTENT_USER_ID));

        @Nullable final User projectRemoved = service.removeById(projectToRemove.getId());
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove, projectRemoved);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectRemoved.getId()));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        @Nullable final User projectToRemove = USER_1;
        projectToRemove.setLogin(USER_LOGIN);
        service.add((projectToRemove));

        Assert.assertThrows(LoginEmptyException.class, () -> service.removeByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByLogin(NON_EXISTENT_USER_LOGIN));

        @Nullable final User projectRemoved = service.removeByLogin(USER_LOGIN);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove, projectRemoved);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectRemoved.getId()));
    }

    @Test
    public void removeByEmail() throws AbstractException {
        @Nullable final User projectToRemove = USER_1;
        projectToRemove.setEmail(USER_EMAIL);
        service.add((projectToRemove));

        Assert.assertThrows(EmailEmptyException.class, () -> service.removeByEmail(NULL_EMAIL));
        Assert.assertThrows(UserNotFoundException.class, () -> service.removeByEmail(NON_EXISTENT_USER_EMAIL));

        @Nullable final User projectRemoved = service.removeByEmail(USER_EMAIL);
        Assert.assertNotNull(projectRemoved);
        Assert.assertEquals(projectToRemove, projectRemoved);

        Assert.assertThrows(EntityNotFoundException.class, () -> service.findOneById(projectRemoved.getId()));
    }

    @Test
    public void isExists() throws AbstractException {
        @NotNull final User projectExists = USER_1;
        service.add(projectExists);

        Assert.assertThrows(IdEmptyException.class, () -> service.existsById(NULL_USER_ID));

        Assert.assertFalse(service.existsById(NON_EXISTENT_USER_ID));
        Assert.assertTrue(service.existsById(projectExists.getId()));
    }

    @Test
    public void isLoginExists() throws AbstractException {
        @NotNull final User projectExists = USER_1;
        projectExists.setLogin(USER_LOGIN);
        service.add(projectExists);

        Assert.assertThrows(LoginEmptyException.class, () -> service.isLoginExists(NULL_LOGIN));

        Assert.assertFalse(service.isLoginExists(NON_EXISTENT_USER_LOGIN));
        Assert.assertTrue(service.isLoginExists(USER_LOGIN));
    }

    @Test
    public void isEmailExists() throws AbstractException {
        @NotNull final User projectExists = USER_1;
        projectExists.setLogin(USER_EMAIL);
        service.add(projectExists);

        Assert.assertThrows(LoginEmptyException.class, () -> service.isLoginExists(NULL_EMAIL));

        Assert.assertFalse(service.isLoginExists(NON_EXISTENT_USER_EMAIL));
        Assert.assertTrue(service.isLoginExists(USER_EMAIL));
    }

    @Test
    public void lockUserByLogin() throws AbstractException {
        @NotNull final User userToUpdate = USER_1;
        userToUpdate.setLocked(false);
        userToUpdate.setLogin(USER_LOGIN);
        service.add(userToUpdate);
        @NotNull final User userNoUpdate = USER_2;
        userNoUpdate.setLocked(false);
        service.add(userNoUpdate);

        Assert.assertThrows(LoginEmptyException.class, () -> service.lockUserByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.lockUserByLogin(NON_EXISTENT_USER_LOGIN));

        service.lockUserByLogin(USER_LOGIN);

        @Nullable final User userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate, userToUpdate);
        Assert.assertTrue(userFindOneByIdToUpdate.isLocked());

        @Nullable final User userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate, userNoUpdate);
        Assert.assertFalse(userFindOneByIdNoUpdate.isLocked());
    }

    @Test
    public void unlockUserByLogin() throws AbstractException {
        @NotNull final User userToUpdate = USER_1;
        userToUpdate.setLocked(true);
        userToUpdate.setLogin(USER_LOGIN);
        service.add(userToUpdate);
        @NotNull final User userNoUpdate = USER_2;
        userNoUpdate.setLocked(true);
        service.add(userNoUpdate);

        Assert.assertThrows(LoginEmptyException.class, () -> service.unlockUserByLogin(NULL_LOGIN));
        Assert.assertThrows(UserNotFoundException.class, () -> service.unlockUserByLogin(NON_EXISTENT_USER_LOGIN));

        service.unlockUserByLogin(USER_LOGIN);

        @Nullable final User userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate, userToUpdate);
        Assert.assertFalse(userFindOneByIdToUpdate.isLocked());

        @Nullable final User userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate, userNoUpdate);
        Assert.assertTrue(userFindOneByIdNoUpdate.isLocked());
    }

    @Test
    public void setPassword() throws AbstractException, NoSuchAlgorithmException {
        @NotNull final User userToUpdate = service.add(USER_1);
        @NotNull final User userNoUpdate = service.add(USER_2);

        Assert.assertThrows(IdEmptyException.class, () -> service.setPassword(NULL_USER_ID, USER_PASSWORD));
        Assert.assertThrows(UserNotFoundException.class, () -> service.setPassword(NON_EXISTENT_USER_ID, USER_PASSWORD));
        Assert.assertThrows(PasswordEmptyException.class, () -> service.setPassword(userToUpdate.getId(), NULL_PASSWORD));

        @Nullable final User userUpdated = service.setPassword(userToUpdate.getId(), USER_PASSWORD);
        Assert.assertNotNull(userUpdated);
        Assert.assertEquals(userUpdated, userToUpdate);
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), userUpdated.getPasswordHash());

        @Nullable final User userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate, userToUpdate);
        Assert.assertEquals(HashUtil.salt(propertyService, USER_PASSWORD), userFindOneByIdToUpdate.getPasswordHash());

        @Nullable final User userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate, userNoUpdate);
        Assert.assertNotEquals(HashUtil.salt(propertyService, USER_PASSWORD), userFindOneByIdNoUpdate.getPasswordHash());
    }

    @Test
    public void updateUser() throws AbstractException {
        @NotNull final User userToUpdate = service.add(USER_1);
        @NotNull final User userNoUpdate = service.add(USER_2);

        Assert.assertThrows(IdEmptyException.class, () -> service.updateUser(NULL_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME));
        Assert.assertThrows(UserNotFoundException.class, () -> service.updateUser(NON_EXISTENT_USER_ID, USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME));
        service.updateUser(userToUpdate.getId(), NULL_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        service.updateUser(userToUpdate.getId(), USER_FIRST_NAME, NULL_MIDDLE_NAME, USER_LAST_NAME);
        service.updateUser(userToUpdate.getId(), USER_FIRST_NAME, USER_MIDDLE_NAME, NULL_LAST_NAME);

        @Nullable final User userUpdated = service.updateUser(userToUpdate.getId(), USER_FIRST_NAME, USER_MIDDLE_NAME, USER_LAST_NAME);
        Assert.assertNotNull(userUpdated);
        Assert.assertEquals(userUpdated, userToUpdate);
        Assert.assertEquals(USER_FIRST_NAME, userUpdated.getFirstName());
        Assert.assertEquals(USER_MIDDLE_NAME, userUpdated.getMiddleName());
        Assert.assertEquals(USER_LAST_NAME, userUpdated.getLastName());

        @Nullable final User userFindOneByIdToUpdate = service.findOneById(userToUpdate.getId());
        Assert.assertNotNull(userFindOneByIdToUpdate);
        Assert.assertEquals(userFindOneByIdToUpdate, userToUpdate);
        Assert.assertEquals(USER_FIRST_NAME, userFindOneByIdToUpdate.getFirstName());
        Assert.assertEquals(USER_MIDDLE_NAME, userFindOneByIdToUpdate.getMiddleName());
        Assert.assertEquals(USER_LAST_NAME, userFindOneByIdToUpdate.getLastName());

        @Nullable final User userFindOneByIdNoUpdate = service.findOneById(userNoUpdate.getId());
        Assert.assertNotNull(userFindOneByIdNoUpdate);
        Assert.assertEquals(userFindOneByIdNoUpdate, userNoUpdate);
        Assert.assertNotEquals(USER_FIRST_NAME, userFindOneByIdNoUpdate.getFirstName());
        Assert.assertNotEquals(USER_MIDDLE_NAME, userFindOneByIdNoUpdate.getMiddleName());
        Assert.assertNotEquals(USER_LAST_NAME, userFindOneByIdNoUpdate.getLastName());
    }

}
