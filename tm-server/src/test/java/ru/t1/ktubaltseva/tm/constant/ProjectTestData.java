package ru.t1.ktubaltseva.tm.constant;

import lombok.experimental.UtilityClass;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.ktubaltseva.tm.enumerated.Status;
import ru.t1.ktubaltseva.tm.model.Project;
import ru.t1.ktubaltseva.tm.model.User;

import java.util.Arrays;
import java.util.List;

@UtilityClass
public final class ProjectTestData {
    @NotNull
    public final static Project PROJECT_1 = new Project();

    @NotNull
    public final static Project PROJECT_2 = new Project();

    @NotNull
    public final static Project PROJECT_3 = new Project();

    @Nullable
    public final static Project NULL_PROJECT = null;

    @NotNull
    public final static List<Project> PROJECT_LIST = Arrays.asList(PROJECT_1, PROJECT_2, PROJECT_3);

    @NotNull
    public final static User USER_1 = new User();

    @NotNull
    public final static User USER_2 = new User();

    @Nullable
    public final static String NULL_USER_ID = null;

    @Nullable
    public final static String NON_EXISTENT_USER_ID = "NON_EXISTENT_USER_ID";

    @Nullable
    public final static String NULL_PROJECT_ID = null;

    @Nullable
    public final static String NON_EXISTENT_PROJECT_ID = "NON_EXISTENT_PROJECT_ID";

    @Nullable
    public final static Project NON_EXISTENT_PROJECT = new Project();

    @NotNull
    public final static Project USER_1_PROJECT_1 = new Project(USER_1);

    @NotNull
    public final static Project USER_1_PROJECT_2 = new Project(USER_1);

    @NotNull
    public final static Project USER_2_PROJECT_1 = new Project(USER_2);

    @NotNull
    public final static Project USER_2_PROJECT_2 = new Project(USER_2);

    @NotNull
    public final static List<Project> USER_1_PROJECT_LIST = Arrays.asList(USER_1_PROJECT_1, USER_1_PROJECT_2);

    @NotNull
    public final static List<Project> USER_2_PROJECT_LIST = Arrays.asList(USER_2_PROJECT_1, USER_2_PROJECT_2);

    @Nullable
    public final static String NULL_NAME = null;

    @Nullable
    public final static String NULL_DESC = null;

    @Nullable
    public final static String PROJECT_NAME = "PROJECT_NAME";

    @Nullable
    public final static String PROJECT_DESC = "PROJECT_DESC";

    @Nullable
    public final static Status NULL_STATUS = null;

    @Nullable
    public final static Status PROJECT_STATUS = Status.COMPLETED;

}
